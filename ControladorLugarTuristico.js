module.exports=function(app){
	return{
		add:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
					connection.release();
					res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
				}
				connection.query("INSERT INTO lugar VALUES (NULL,'"+req.body.nombre+"','"+req.body.descripcion+"','"+req.body.idDepartamento+"');", function(err,row){
				if(err)
					throw err;
				else
					res.json({"mensaje":"Lugar Agregado"});
				connection.release();
				});
			});
		},
				delete:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Delete from lugarturistico where idLugarTuristico="+req.body.idLugarTuristico, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Lugar Turistico eliminado"});
					connection.release();	
				});
			});	
		},
		list:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Select * from lugarturistico where idLugarTuristico="+req.query.idLugarTuristico, function(err, row){
					if(err)
						throw err;
					else
						res.json(row);
					connection.release();	
				});
			});	
		},
		edit:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("UPDATE lugarturistico set nombre='"+req.body.nombre+"',descripcion="+req.body.descripcion+",idDepartamento="+req.body.idDepartamento+"' where idLugarTuristico="+req.body.idLugarTuristico, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Lugar Turistico editado"});
					connection.release();	
				});
			});	
		}
	}
}