module.exports=function(app){
	return{
		add:function(req,res){
			pool.getConnection(function(err,connection){
				if(err){
					connection.release();
					res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
				}
				connection.query("INSERT INTO tipohabitacion VALUES (NULL,'"+req.body.nombre+"','"+req.body.descripcion+"');", function(err,row){
				if(err)
					throw err;
				else
					res.json({"mensaje":"Tipo de Habitacion Agregada"});
				connection.release();
				});
			});
		},
				delete:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Delete from tipohabitacion where idTipoHabitacion="+req.body.idTipoHabitacion, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Tipo de Habitacion eliminada"});
					connection.release();	
				});
			});	
		},
		list:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Select * from tipohabitacion where idTipoHabitacion="+req.query.idTipoHabitacion, function(err, row){
					if(err)
						throw err;
					else
						res.json(row);
					connection.release();	
				});
			});	
		},
		edit:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("UPDATE tipohabitacion set nombre='"+req.body.nombre+"',descripcion="+req.body.descripcion+"' where idLugarTuristico="+req.body.idLugarTuristico, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Tipo de Habitacion editado"});
					connection.release();	
				});
			});	
		}
	}
}