module.exports=function(app){
	return{
		add:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
					connection.release();
					res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
				}
				connection.query("INSERT INTO departamento VALUES (NULL,'"+req.body.nombre+"');", function(err,row){
				if(err)
					throw err;
				else
					res.json({"mensaje":"Departamento Agregado"});
				connection.release();
				});
			});
		},
				delete:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Delete from departamento where idDepartamento="+req.body.idDepartamento, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Departamento eliminado"});
					connection.release();	
				});
			});	
		},
		list:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Select * from departamento where idDepartamento="+req.query.idDepartamento, function(err, row){
					if(err)
						throw err;
					else
						res.json(row);
					connection.release();	
				});
			});	
		},
		edit:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("UPDATE departamento set nombre='"+req.body.nombre+"' where idDepartamento="+req.body.idDepartamento, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Departamento editado"});
					connection.release();	
				});
			});	
		}
	}
}